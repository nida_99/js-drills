const listCarYear = require("./problem4.js");
function listOldCars(inventory) {
    let array = listCarYear(inventory);
    let outputArray = [];
    for (let index = 0; index < array.length; index++) {
        if (array[index] < 2000) {
            outputArray.push(array[index]);
        }
    };
    return outputArray.length;
};


module.exports.listOldCars = listOldCars;