const getInfoOfId = require("../problem1.js");
const inventory = require(("../inventory.js"));

const expectedOutput = "car 33 is a 2011 Jeep Wrangler"
let findId = 33;
const actualOutput = getInfoOfId(inventory, findId);

if (actualOutput === expectedOutput) {
    console.log("Function is working properly");
}
else {
    console.log("Check again");
};



