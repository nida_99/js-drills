const listCarYear  = require("../problem4.js").listCarYear;
const listOldCars = require("../problem5.js").listOldCars;
const inventory = require(("../inventory.js"));

const expectedOutput = 25;
const actualOutput = listOldCars(inventory);

if(actualOutput === expectedOutput){
    console.log("Function is working properly");
}else
{
    console.log("Check the function again!");
};

