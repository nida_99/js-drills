function getInfoLast(inventory) {
    let total = inventory.length - 1;
    let car_make = inventory[total].car_make;
    let car_model = inventory[total].car_model;
    return `Last car is a ${car_make} ${car_model}`;
};

module.exports = getInfoLast;