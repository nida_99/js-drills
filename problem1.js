function getInfoOfId(inventory, findId) {
    for (let index = 0; index < inventory.length; index++) {
        if (inventory[index].id === findId) {
            let car_make = inventory[index].car_make;
            let car_model = inventory[index].car_model;
            let car_year = inventory[index].car_year;
            return `car ${findId} is a ${car_year} ${car_make} ${car_model}`;
        };
    };
};
module.exports = getInfoOfId;