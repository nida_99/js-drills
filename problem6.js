function listBmwAndAudi(inventory) {
    let outputArray = [];
    for (let index = 0; index < inventory.length; index++) {
        if (inventory[index].car_make == "Audi" || inventory[index].car_make == "BMW") {
            outputArray.push(inventory[index]);
        }
    };
    return outputArray;
};
module.exports = listBmwAndAudi;