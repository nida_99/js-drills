const inventory = require("./inventory");
function sortAlphabetically(inventory) {
    let carModelArray = [];

    for (let index = 0; index < inventory.length; index++) {
        carModelArray.push(inventory[index].car_model.toLowerCase());

    }
    for (let i = 1; i < inventory.length; i++) {
        let j = i - 1;
        let temp = carModelArray[i];

        while (j >= 0 && carModelArray[j] && carModelArray[j] > temp) {
            carModelArray[j + 1] = carModelArray[j];
            j--;
        }
        carModelArray[j + 1] = temp;
    }
    let outputArray = [];
    for (let j = 0; j < inventory.length; j++) {

        let value = inventory.findIndex(inventory => inventory.car_model.toLowerCase() === carModelArray[j]);
        outputArray.push(inventory[value]);
    };
    return outputArray;
};
module.exports.sortAlphabetically = sortAlphabetically;
