function listCarYear(inventory) {
    let outputArray = [];
    for (let index = 0; index < inventory.length; index++) {
        outputArray.push(inventory[index].car_year);
    };
    return outputArray;
};

module.exports = listCarYear;
